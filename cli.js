#!/usr/bin/env node --no-warnings

/*
 * This file is licensed to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

const fsp = require('fs').promises;
const path = require('path');
const os = require('os');
const yargs = require('yargs');
const m3u8Parser = require('m3u8-parser');
const mm = require('music-metadata');
const ejs = require('ejs');
const webpackConfigFile = path.join(__dirname, 'webpack.config.js');
const webpackConfig = require(webpackConfigFile)(process.env.NODE_ENV);

const {statExists} = require('./lib/fs.js');
const {formatDuration} = require('./lib/util.js');
const tasks = require('./lib/tasks.js');
const Make = require('./lib/make.js');

const argv = yargs.command(['generate <playlist>', '$0'], 'Generate site', (yargs) => {
    yargs.positional('playlist', {
      describe: 'M3U playlist to generate site from',
      type: 'string'
    }).option('output-dir', {
      alias: 'o',
      describe: 'Directory to output static site to',
      type: 'string'
    }).option('album-title', {
      alias: 't',
      describe: 'Custom album title',
      type: 'string'
    }).demandOption(['output-dir'],
      'Please provide a valid M3U playlist file and output directory');
  }).option('no-download', {
    alias: 'n',
    describe: "Don't include download links",
    type: 'boolean',
    default: false
  })
  .argv;

const indexTemplateFileName = path.join(__dirname, 'src/index.ejs');

const {outputDir, noDownload, playlist, albumTitle} = argv;
const iconPath = path.join(outputDir, 'icons')
const trackPath = path.join(outputDir, 'tracks');
const tmpPath = path.join(os.tmpdir(), `ssav-generator-${path.basename(playlist)}`);

const FILE_PREFIX = 'file://';
(async () => {
  const indexTemplate = ejs.compile(
    await fsp.readFile(indexTemplateFileName, 'utf-8'),
    {filename: indexTemplateFileName});

  const make = new Make;
  [outputDir, iconPath, trackPath, tmpPath].forEach(d => make.add(d, tasks.mkdir));

  const metadata = path.join(tmpPath, 'metadata.json');
  const cssIn = path.join(__dirname, 'src', 'style.css');
  const cssOut = path.join(outputDir, 'style.css');
  make.add(cssOut, [cssIn, outputDir, tasks.copyFile]);
  const jsBundle = path.join(outputDir, webpackConfig.output.filename);
  const jsIn = webpackConfig.entry.map(f => path.normalize(path.join(__dirname, f)));
  webpackConfig.output.path = outputDir;
  webpackConfig.resolve.alias[path.basename(metadata)] = metadata;
  make.add(jsBundle, [...jsIn, metadata, webpackConfigFile, outputDir, tasks.webpackCompile(webpackConfig, jsIn)]);

  const iconInPath = path.join(__dirname, 'icons');
  const iconsOut = [];
  for (const icon of await fsp.readdir(iconInPath)) {
    const iconOut = path.join(iconPath, icon);
    make.add(iconOut, [path.join(iconInPath, icon), iconPath, tasks.copyFile]);
    iconsOut.push(iconOut);
  }

  make.add('all', [cssOut, jsBundle, ...iconsOut], {phony: true});

  if (!(await statExists(playlist))) {
    console.error(`Invalid playlist file: ${playlist}`);
    process.exit(1);
  }
  const manifest = await fsp.readFile(playlist, 'utf-8');
  const parser = new m3u8Parser.Parser();
  parser.push(manifest);
  parser.end();

  const segments = (await Promise.all(parser.manifest.segments.map(async ({uri}) => {
    uri = decodeURI(uri);
    if (uri && uri.startsWith(FILE_PREFIX)) {
      uri = uri.substr(FILE_PREFIX.length);
    } else if (uri && !uri.startsWith('/')) {
      uri = path.join(path.dirname(playlist), uri);
    }
    if (!(await statExists(uri))) {
      console.error(`WARNING: skipping invalid file ${uri}`);
      return null;
    }
    const fileName = path.basename(uri);
    return {
      fileName,
      uri
    };
  }))).filter(r => r);

  if (!segments.length) {
    console.error(`No valid audio files in playlist: ${playlist}`);
    process.exit(1);
  }

  let trackNum = 0;
  const trackNumZeroes = segments.length.toString().length;
  const tracks = [];
  let firstTrack = null;
  let album = albumTitle || null;
  let albumUri = null;
  for (const segment of segments) {
    trackNum += 1;
    segment.num = trackNum;
    const ext = path.extname(segment.fileName);
    const metadata = await mm.parseFile(segment.uri);
    const title = metadata.common.title || path.basename(segment.fileName, ext);
    if (album === null) {
      album = metadata.common.album;
    } else if (album !== metadata.common.album) {
      album = albumTitle;
    }
    const trackNumString = segment.num.toString().padStart(trackNumZeroes, '0');
    const outFileName = `${trackNumString} - ${title}${ext}`;
    const track = {
      title,
      num: trackNum,
      uri: `tracks/${outFileName}`,
      artist: metadata.common.artist,
      duration: formatDuration(metadata.format.duration),
      playerUri: `${trackNumString}.${title}.html`
    };
    segment.localPath = path.join(trackPath, outFileName);

    tracks.push(track);
    const trackPlayer = path.join(outputDir, track.playerUri);
    if (!firstTrack) {
      firstTrack = trackPlayer;
    }
    make.add(segment.localPath, [segment.uri, trackPath, tasks.copyFile]);
    make.add(trackPlayer, [segment.uri, indexTemplateFileName, outputDir,
             tasks.render(() => indexTemplate({
               albumTitle: album,
               albumUri,
               currentTrack: track,
               download: !noDownload,
               tracks
             }))]);
    make.add('all', [segment.localPath, trackPlayer]);

    segment.metadataPath = path.join(tmpPath, `${path.basename(segment.fileName, ext)}.json`);
    make.add(segment.metadataPath, [segment.uri, tmpPath, tasks.generateMetadata]);
  }
  if (!album) {
    album = 'Album';
  }
  albumUri = `${album}.zip`;

  const albumFile = path.join(outputDir, albumUri);
  const trackFiles = segments.map(s => s.localPath);
  make.add(albumFile, [...trackFiles, outputDir, tasks.zipFiles(trackFiles)]);
  make.add('all', albumFile);

  const metadataFiles = segments.map(s => s.metadataPath);
  make.add(metadata, [...metadataFiles, tmpPath, tasks.combineMetadata(metadataFiles)]);

  if (firstTrack) {
    const indexFileName = path.join(outputDir, 'index.html');
    make.add(indexFileName, [firstTrack, outputDir, tasks.copyFile]);
    make.add('all', indexFileName);
  }

  await make.exec('all');
})().catch(e => {
  console.error(e);
  process.exit(1);
});

