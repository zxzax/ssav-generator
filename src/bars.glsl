precision mediump float;

uniform vec2 screenSize;
uniform sampler2D data;
uniform sampler2D freq;
uniform float time;
uniform float duration;
uniform float globalTime;

varying vec2 texCoord;

#define BAR_SIZE 2.0

void main(void) {
  float px = texCoord.x * screenSize.x;
  float rx = floor(px / BAR_SIZE) / (screenSize.x / BAR_SIZE);
  float val = texture2D(freq, vec2(rx, 0.0)).r;
#ifdef FREQ_USE_FLOAT
  val = 1.0 - (val - MAX_DECIBELS) / (MIN_DECIBELS - MAX_DECIBELS);
#endif
  if (val < texCoord.y) {
    val = 0.0;
  } else {
    //val = (3.0/16.0) + (val - texCoord.y) / val * (12.0/16.0);
    val = texCoord.y * (9.0/16.0) + (4.0/16.0);
  }
  if (mod(px, BAR_SIZE * 2.0) > BAR_SIZE) val = 0.0;
  gl_FragColor = vec4(1.0, 1.0, 1.0, val);
}
