precision mediump float;

uniform vec2 screenSize;
uniform sampler2D data;
uniform sampler2D freq;
uniform float time;
uniform float duration;
uniform float globalTime;

varying vec2 texCoord;

void main(void) {
  float val = texture2D(data, vec2(texCoord.x, 0.0)).r;
#ifdef DATA_USE_FLOAT
  val = val * 0.5 + 0.5;
#endif
  val = sqrt(clamp((15.0/16.0) - clamp(abs(texCoord.y - val) * 64.0, 0.0, 1.0), 0.0, 15.0/16.0));
  gl_FragColor = vec4(1.0, 1.0, 1.0, val);
}
