// @license  magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
/* Copyright © 2018-2020 Jason Francis */
/*
 * This file is licensed to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {clamp, formatDuration, mapObject, shuffle} from '../lib/util.js';
import {createElement as E} from './framework.js';
import VERTEX_SHADER from './vertex.glsl';
import BARS_SHADER from './bars.glsl';
import SCOPE_SHADER from './scope.glsl';
import INFO_MARKUP from './info.html';
import METADATA from 'metadata.json';

const DEFAULT_PROGRAMS = Object.freeze({
  bars: BARS_SHADER,
  scope: SCOPE_SHADER
});

const body = document.getElementById('body');
const audio = document.getElementById('audio');
const playlistContainer = document.getElementById('playlist-container');
const canvas = document.getElementById('viz');
playlistContainer.classList.add('docked', 'hidden');
audio.controls = false;

const elems = {};
const tracks = [];
let trackOrderIndex = 0;
let currentTrackElem = null;
let trackCount = 0;
let trackOrder = [];
let volumeClass = 'volume-high';
let dragging = null;
let hidden = false;
let sliderGutterRect = null;
let volumeGutterRect = null;
let settingsLoaded = false;

const gl = canvas.getContext('webgl', { depth: false, alpha: true, antialias: true });
const textureFloatExt = gl && gl.getExtension('OES_texture_float');
const textureFloatLinearExt = gl && gl.getExtension('OES_texture_float_linear');
const programs = {};
const vizOptions = [];
let currentProgram = 'bars';
let lastProgram = currentProgram || 'bars';
let dataTexture = null;
let freqTexture = null;
let vizRunning = true;

const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
const playerSource = audioCtx.createMediaElementSource(audio);
const gainNode = audioCtx.createGain();
const analyser = audioCtx.createAnalyser();
analyser.fftSize = 2048;
const frequencyBinCount = analyser.frequencyBinCount;
const dataArray = new (textureFloatExt && analyser.getFloatTimeDomainData ? Float32Array : Uint8Array)(frequencyBinCount * frequencyBinCount);
const freqArray = new (textureFloatExt && analyser.getFloatFrequencyData ? Float32Array : Uint8Array)(frequencyBinCount * frequencyBinCount);

const WAVEFORM_HEIGHT = 48;

const SettingsKeys = Object.freeze({
  VOLUME: 'ssav-volume',
  MUTE: 'ssav-mute',
  VIZ: 'ssav-viz',
  SHUFFLE: 'ssav-shuffle',
  REPEAT: 'ssav-repeat'
});

const BINDS = {
  ArrowLeft: () => audio.currentTime -= 5,
  ArrowRight: () => audio.currentTime += 5,
  ArrowUp: volumeUp,
  ArrowDown: volumeDown,
  KeyD: () => elems.downloadButton.click(),
  Escape: toggleControls,
  KeyI: toggleInfo,
  KeyM: toggleMute,
  KeyP: togglePlaylist,
  KeyR: cycleRepeat,
  KeyS: toggleShuffle,
  KeyV: cycleViz,
  Comma: previous,
  Period: next,
  Space: playOrPause
};

if (!PRODUCTION) {
  BINDS.KeyL = () => {
    if (!currentProgram) {
      return;
    }
    const program = currentProgram; // don't let this change
    fetch(`${program}.glsl`)
      .then(r => r.ok ? r.text() : Promise.reject(r))
      .then(src => loadProgram(program, src));
  };
}

for (const a of playlistContainer.querySelectorAll('.track-button')) {
  const li = a.parentNode;
  const track = {
    id: trackCount,
    uri: a.dataset.audio,
    playerUri: a.getAttribute('href'),
    title: a.querySelector('.title').innerText,
    artist: (a.querySelector('.artist') || {}).innerText,
    downloadable: li.classList.contains('downloadable'),
    waveform: null,
    waveformBlob: null,
    playlistElem: li
  };
  tracks.push(track);
  a.removeAttribute('href');
  if (li.classList.contains('selected')) {
    currentTrackElem = li;
    trackOrderIndex = trackCount;
  }
  a.addEventListener('click', e => {
    changeSong(trackOrder.indexOf(track.id));
    e.preventDefault();
  });
  trackOrder.push(trackCount);
  trackCount += 1;
}

const player = E('div', {'class': 'player'},
           elems.previousButton = E('button', {id:'previous', 'class':'previous', title: 'Previous (,)',  onClick: previous}),
           elems.playButton = E('button', {id:'play-pause', 'class':'loading', title: 'Play/Pause (Space)', onClick: playOrPause}),
           elems.nextButton = E('button', {id:'next', 'class':'next', title: 'Next (.)', onClick: next}),
           E('div', {'class':'vertical-container'},
             E('div', {'class': 'menu-button-container'},
               elems.volumeButton = E('button', {'class': 'small', title: 'Volume (\u2191, \u2193, M)', onClick: toggleMute}),
               elems.volumeMenu = E('div', {'class': 'menu-container'},
                 E('div', {'class': 'menu volume-menu', onMouseDown: startVolumeDrag},
                   elems.volumeGutter = E('div', {id: 'volume-gutter'},
                     elems.volumeFill = E('div', {id: 'volume-fill'}))))),
             E('div', {'class': 'menu-button-container'},
               elems.vizButton = E('button', {'class': 'small viz', title: 'Visualization (V)', onClick: toggleViz}),
               E('div', {'class': 'menu-container'},
                 elems.vizMenu = E('ol', {'class': 'menu viz-menu'},
                   E('li', {}, 'None'),
                   E('li', {'data-program': 'bars'}, 'FFT'),
                   E('li', {'data-program': 'scope'}, 'Scope'))))),
           elems.currentTime = E('div', {'class': 'duration'}),
           elems.sliderContainer = E('div', {id:'slider-container', onMouseDown: startSliderDrag},
             elems.seekPopupContainer = E('div', {'class': 'seek-popup-container'},
               elems.seekPopup = E('div', {'class': 'seek-popup'})),
             elems.marquee = E('div', {id: 'player-marquee'},
               elems.marqueeTitle = E('div', {id: 'marquee-title'}),
               elems.marqueeArtist = E('div', {id: 'marquee-artist'})),
             elems.sliderGutter = E('div', {id: 'slider-gutter'},
               elems.sliderWaveform = E('div', {id: 'slider-waveform'}),
               elems.sliderFill = E('div', {id: 'slider-fill'}))),
           elems.songDuration = E('div', {'class': 'duration'}),
           E('div', {'class':'vertical-container'},
             elems.repeatButton = E('button', {'class': 'small active repeat', title: 'Repeat (R)', onClick: cycleRepeat}),
             elems.shuffleButton = E('button', {'class': 'small shuffle', title: 'Shuffle (S)', onClick: toggleShuffle})),
           E('div', {'class':'vertical-container'},
             elems.hideButton = E('button', {'class': 'small up', title: 'Hide Controls (Escape)', onClick: toggleControls}),
             elems.infoButton = E('button', {'class': 'small info', title: 'Info (I)', onClick: toggleInfo})),
           elems.downloadButton = E('a', {'class':'button download', title: 'Download Track (D)', download: ''}),
           elems.playlistButton = E('button', {'class':'playlist', title: 'Playlist (P)', onClick: togglePlaylist})
          );
body.appendChild(player);

const infoWindow = E('section', {id: 'info', 'class': 'window hidden'},
                     E('header', {'class': 'title'}, 'Info'),
                     E('button', {'class': 'close', title: 'Close', onClick: toggleInfo}),
                     elems.infoContent = E('div', {'class': 'content'}));
elems.infoContent.innerHTML = INFO_MARKUP;
body.appendChild(infoWindow);

window.addEventListener('resize', updateRects);
window.addEventListener('keydown', globalKey, true);
window.addEventListener('mousedown', mouseDown);
window.addEventListener('wheel', globalWheel, true);
window.addEventListener('mouseup', mouseUp);
window.addEventListener('mousemove', mouseMove);
audio.addEventListener('canplay', setup);
audio.addEventListener('loadedmetadata', setup);
audio.addEventListener('durationchange', setup);
audio.addEventListener('pause', pauseEvent);
audio.addEventListener('play', playEvent);
audio.addEventListener('ended', songEnded);
audio.addEventListener('timeupdate', updatePosition);
for (let i = 0; i < elems.vizMenu.childElementCount; i += 1) {
  const vizOption = elems.vizMenu.children.item(i);
  vizOptions.push(vizOption);
  vizOption.addEventListener('click', () => selectVizOption(vizOption));
  const {program} = vizOption.dataset;
  if (program === currentProgram) {
    selectVizOption(vizOption);
  }
}

updateCurrent(tracks[trackOrder[trackOrderIndex]]);
setup();
updateRects();
updateVolume();
playerSource.connect(gainNode);
gainNode.connect(analyser);
analyser.connect(audioCtx.destination);
setupGL();
requestAnimationFrame(draw);
if (window.localStorage) {
  loadSettings(mapObject(SettingsKeys, v => {
    try {
      return JSON.parse(window.localStorage.getItem(v));
    } catch(e) {
      return null;
    }
  }));
}

function loadSettings(settings) {
  if (settings.REPEAT !== null) {
    const repeat = getRepeatMode();
    if (repeat !== settings.REPEAT) {
      setRepeatMode(settings.REPEAT);
    }
  }
  if (settings.SHUFFLE !== null && Boolean(shuffleEnabled()) !== Boolean(settings.SHUFFLE)) {
    toggleShuffle();
  }
  if (settings.MUTE !== null) {
    audio.muted = Boolean(settings.MUTE);
  }
  if (settings.VOLUME !== null) {
    gainNode.gain.value = settings.VOLUME;
  }
  updateVolume();
  if (settings.VIZ !== null) {
    const program = String(settings.VIZ);
    selectViz(program === 'none' ? null : program);
  }
  settingsLoaded = true;
}
function saveSetting(k, v) {
  if (settingsLoaded && window.localStorage) {
    window.localStorage.setItem(k, JSON.stringify(v));
  }
  return Promise.resolve();
}
function globalKey(e) {
  const bind = BINDS[e.code];
  if (bind) {
    bind();
    if (!e.ctrlKey && !e.metaKey && !e.altKey && !e.shiftKey) {
      e.stopPropagation();
      e.preventDefault();
    }
  }
}
function setup() {
  if (audio.readyState >= 2) {
    if (!vizRunning) {
      draw();
    }
    elems.playButton.classList.remove('play', 'pause', 'loading');
    elems.playButton.classList.add(audio.paused ? 'play' : 'pause');
  }
  if (audio.readyState >= 1) {
    elems.songDuration.innerText = formatDuration(audio.duration);
  }
}
function pauseEvent() {
  elems.playButton.classList.remove('pause');
  elems.playButton.classList.add('play');
}
function playEvent() {
  elems.playButton.classList.remove('play', 'loading');
  elems.playButton.classList.add('pause');
  if (!vizRunning) {
    draw();
  }
}
function updatePosition() {
  elems.currentTime.innerText = formatDuration(audio.currentTime);
  const percent = `${audio.currentTime / audio.duration * 100}%`;
  elems.sliderFill.style.width = percent;
}
function changeSong(index) {
  const track = tracks[trackOrder[index]];
  if (currentTrackElem) {
    currentTrackElem.classList.remove('selected');
  }
  const state = Object.assign({}, track);
  delete state.playlistElem;
  window.history.replaceState(state, state.title, `./${track.playerUri}`);
  window.document.title = track.title;
  trackOrderIndex = index;
  audio.src = track.uri;
  audio.currentTime = 0;
  audio.play();
  track.playlistElem.classList.add('selected');
  currentTrackElem = track.playlistElem;
  updateCurrent(track);
  updatePosition();
  elems.playButton.classList.add('loading');
  if (!vizRunning) {
    draw();
  }
}
function updateCurrent(track) {
  displayWaveform(track);
  elems.marqueeTitle.innerText = track.title || '';
  elems.marqueeArtist.innerText = track.artist || '';
  const db = elems.downloadButton;
  db.style.display = track.downloadable ? 'block' : 'none';
  if (track.downloadable) {
    db.setAttribute('download', '');
    db.setAttribute('href', track.uri);
  } else {
    db.removeAttribute('href');
    db.removeAttribute('download');
  }
  const lowerTrack = track.uri.toLowerCase();
  if (lowerTrack.endsWith('ogg')) {
    audio.setAttribute('type', 'audio/ogg');
  } else if (lowerTrack.endsWith('mp3')) {
    audio.setAttribute('type', 'audio/mpeg');
  } else {
    audio.removeAttribute('type');
  }
}
window.audioCtx = audioCtx;
function playOrPause() {
  if (audio.paused) {
    audioCtx.resume();
    audio.play();
    if (!vizRunning) {
      draw();
    }
  } else {
    audio.pause();
  }
}
function previous() {
  changeSong((trackOrderIndex + trackCount - 1) % trackCount);
}
function songEnded() {
  const repeat = getRepeatMode();
  if (repeat !== 'one' && (repeat || trackOrderIndex + 1 < trackCount)) {
    next();
  }
  dragging = null;
}
function next() {
  changeSong((trackOrderIndex + 1) % trackCount);
}
function updateRects() {
  sliderGutterRect = elems.sliderGutter.getBoundingClientRect();
  volumeGutterRect = elems.volumeGutter.getBoundingClientRect();
  elems.sliderWaveform.style.backgroundSize = `${sliderGutterRect.width}px 48px`;
  if (gl) {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    gl.viewport(0, 0, canvas.width, canvas.height);
  }
}
function updateVolume() {
  volumeClass = makeVolumeClass(gainNode.gain.value);
  const cl = elems.volumeButton.classList;
  for (let i = 0; i < cl.length; i += 1) {
    const c = cl.item(i);
    if (c !== 'small') {
      cl.remove(c);
    }
  }
  if (audio.muted) {
    elems.volumeButton.classList.add('volume-mute');
  } else {
    elems.volumeButton.classList.add(volumeClass);
  }
  const height = audio.muted ? 0 : gainNode.gain.value * volumeGutterRect.height;
  elems.volumeFill.style.height = `${height}px`;
  saveSetting(SettingsKeys.VOLUME, gainNode.gain.value);
  saveSetting(SettingsKeys.MUTE, audio.muted);
}
function mouseMove(e) {
  if(e.target === elems.sliderContainer) {
    const {left, width} = sliderGutterRect;
    const destTime = clamp((e.clientX - left) / width, 0, 1) * audio.duration;
    elems.seekPopup.innerText = formatDuration(destTime);
    elems.seekPopupContainer.style.left = `${e.clientX}px`;
  }
  if (!dragging) {
    return;
  }
  if (dragging === 'volume') {
    const style = window.getComputedStyle(elems.volumeMenu);
    if (parseInt(style.opacity, 10) === 0) {
      dragging = null;
      return;
    }
    const {top, height} = volumeGutterRect;
    gainNode.gain.value = 1 - clamp((e.clientY - top) / height, 0, 1);
    audio.muted = false;
    updateVolume();
  } else if (dragging === 'seek') {
    const {left, width} = sliderGutterRect;
    audio.currentTime = clamp((e.clientX - left) / width, 0, 1) * audio.duration;
  }
  e.preventDefault();
  e.stopPropagation();
}
function globalWheel(e) {
  if (e.target === canvas) {
    if (e.deltaY) {
      gainNode.gain.value = clamp(gainNode.gain.value - e.deltaY / 1000, 0, 1);
      updateVolume();
    }
    if (e.deltaX) {
      audio.currentTime += e.deltaX / 100;
    }
  }
}
function mouseDown(e) {
  if (player.classList.contains('fullscreen')) {
    toggleControls();
    e.stopPropagation();
    e.preventDefault();
  } else if (e.target === canvas) {
    playOrPause();
  }
}
function mouseUp(e) {
  if (e.which === 1) {
    dragging = null;
  }
}
function startSliderDrag(e) {
  if (e.which === 1) {
    dragging = 'seek';
    mouseMove(e);
  }
}
function startVolumeDrag(e) {
  if (e.which === 1) {
    dragging = 'volume';
    mouseMove(e);
  }
}
function cycleRepeat() {
  const repeat = getRepeatMode();
  const cl = elems.repeatButton.classList;
  const MODES = Object.freeze([false, true, 'one']);
  const newRepeat = MODES[(MODES.indexOf(repeat) + 1) % MODES.length];
  setRepeatMode(newRepeat);
  saveSetting(SettingsKeys.REPEAT, newRepeat);
}
function setRepeatMode(repeat) {
  const cl = elems.repeatButton.classList;
  cl.remove('active', 'repeat', 'repeat-one');
  if (repeat === 'one') {
    audio.loop = true;
    cl.add('active', 'repeat-one');
  } else {
    audio.loop = false;
    cl.add('repeat');
    if (repeat) {
      cl.add('active');
    }
  }
}
function getRepeatMode() {
  const cl = elems.repeatButton.classList;
  if (cl.contains('active')) {
    if (cl.contains('repeat-one')) {
      return 'one';
    }
    return true;
  }
  return false;
}
function toggleShuffle() {
  elems.shuffleButton.classList.toggle('active');
  const lastId = trackOrder[trackOrderIndex];
  if (shuffleEnabled()) {
    shuffle(trackOrder);
  } else {
    trackOrder.sort((a, b) => a - b);
  }
  trackOrderIndex = trackOrder.indexOf(lastId);
  saveSetting(SettingsKeys.SHUFFLE, shuffleEnabled());
}
function shuffleEnabled() {
  return elems.shuffleButton.classList.contains('active');
}
function makeVolumeClass(volume) {
  if (volume > 2/3) return 'volume-high';
  if (volume > 1/3) return 'volume-medium';
  if (volume > 0) return 'volume-low';
  return 'volume-mute';
}
function volumeUp() {
  gainNode.gain.value = clamp(gainNode.gain.value + 0.05, 0, 1);
  updateVolume();
}
function volumeDown() {
  gainNode.gain.value = clamp(gainNode.gain.value - 0.05, 0, 1);
  updateVolume();
}
function toggleMute() {
  audio.muted = !audio.muted;
  updateVolume();
}
function toggleViz() {
  const cl = elems.vizButton.classList;
  cl.toggle('active');
  selectViz(cl.contains('active') ? lastProgram : null);
}
function selectViz(program) {
  for (const o of vizOptions) {
    const p = o.dataset.program || null;
    if (program === p) {
      selectVizOption(o);
      break;
    }
  }
}
function selectVizOption(option) {
  currentProgram = option.dataset.program || null;
  if (!vizRunning) {
    draw({force: true});
  }
  if (currentProgram) {
    lastProgram = currentProgram;
    elems.vizButton.classList.add('active');
  } else {
    elems.vizButton.classList.remove('active');
  }
  for (const o of vizOptions) {
    if (o === option) {
      o.classList.add('selected');
    } else {
      o.classList.remove('selected');
    }
  }
  saveSetting(SettingsKeys.VIZ, currentProgram || 'none');
}
function cycleViz() {
  let index = 1;
  for (const o of vizOptions) {
    const p = o.dataset.program || null;
    if (currentProgram === p) {
      break;
    }
    index += 1;
  }
  index = index % vizOptions.length;
  selectVizOption(vizOptions[index]);
}
function toggleControls() {
  const cl = elems.hideButton.classList;
  cl.toggle('active');
  if (cl.contains('active')) {
    player.classList.add('fullscreen');
    playlistContainer.classList.add('fullscreen');
    body.classList.add('fullscreen');
  } else {
    player.classList.remove('fullscreen');
    playlistContainer.classList.remove('fullscreen');
    body.classList.remove('fullscreen');
  }
}
function toggleInfo() {
  elems.infoButton.classList.toggle('active');
  infoWindow.classList.toggle('hidden');
}
function togglePlaylist() {
  elems.playlistButton.classList.toggle('active');
  playlistContainer.classList.toggle('hidden');
}
function compileShader(name, type, src) {
  const shader = gl.createShader(type);
  gl.shaderSource(shader, src);
  gl.compileShader(shader);
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    throw new Error(`${name}: ${gl.getShaderInfoLog(shader)}`);
    return null;
  }
  return shader;
}
function setUniform({type, obj}, value) {
  if (type[0] === 'M') {
    gl[`uniform${type}`](obj, false, value);
  } else {
    gl[`uniform${type}`](obj, value);
  }
}
function createProgram({name, shaders, uniforms={}, attribs=[], defines={}}) {
  const program = gl.createProgram();
  for (const [index, {type, src}] of Object.entries(shaders)) {
    gl.attachShader(program, compileShader(`${name} shader #${index}`, type,
      [...Object.entries(defines).map(([n, v]) => `#define ${n} ${v}`), src].join('\n')
    ));
  }
  gl.linkProgram(program);
  gl.validateProgram(program);
  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    throw new Error(`program ${name}: ${gl.getProgramInfoLog(program)}`);
  } else {
    program.uniforms = {};
    const uniformEntries = Object.entries(uniforms);
    if (uniformEntries.length || attribs.length) {
      gl.useProgram(program);
    }
    for (const [name, {type, initial}] of uniformEntries) {
      const uniform = program.uniforms[name] = {
        obj: gl.getUniformLocation(program, name),
        type,
        initial
      };
      if (typeof initial !== 'undefined' && initial !== null) {
        setUniform(uniform, initial);
      }
    }
    program.attribs = {};
    for (const attrib of attribs) {
      program.attribs[attrib] = gl.getAttribLocation(program, attrib);
    }
  }
  return program;
};
function setupGL() {
  if (!gl) return;
  gl.clearColor(0.0, 0.0, 0.0, 0.0);
  gl.enable(gl.BLEND);
  gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
  const screenQuadBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, screenQuadBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
    -1, -1,   -1, 1,   1, -1,   -1, 1,   1, 1,   1, -1
  ]), gl.STATIC_DRAW);

  gl.activeTexture(gl.TEXTURE0);
  dataTexture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, dataTexture);
  let filter = textureFloatExt && analyser.getFloatTimeDomainData && !textureFloatLinearExt ? gl.NEAREST : gl.LINEAR;
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, filter);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, filter);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  if (textureFloatExt && analyser.getFloatTimeDomainData) {
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, 1, 1, 0, gl.LUMINANCE, gl.FLOAT, new Float32Array([0]));
  } else {
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, 1, 1, 0, gl.LUMINANCE, gl.UNSIGNED_BYTE, new Uint8Array([0]));
  }
  gl.activeTexture(gl.TEXTURE1);
  freqTexture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, freqTexture);
  filter = textureFloatExt && analyser.getFloatFrequencyData && !textureFloatLinearExt ? gl.NEAREST : gl.LINEAR;
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, filter);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, filter);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  if (textureFloatExt && analyser.getFloatFrequencyData) {
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, 1, 1, 0, gl.LUMINANCE, gl.FLOAT, new Float32Array([0]));
  } else {
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, 1, 1, 0, gl.LUMINANCE, gl.UNSIGNED_BYTE, new Uint8Array([0]));
  }

  for (const [name, src] of Object.entries(DEFAULT_PROGRAMS)) {
    loadProgram(name, src);
  }
  gl.enableVertexAttribArray(programs.bars.attribs.position);
  gl.vertexAttribPointer(programs.bars.attribs.position, 2, gl.FLOAT, false, 2*4, 0);
}
function loadProgram(name, src) {
  const defines = {
    MIN_DECIBELS: analyser.minDecibels.toPrecision(5),
    MAX_DECIBELS: analyser.maxDecibels.toPrecision(5)
  };
  if (textureFloatExt && analyser.getFloatTimeDomainData) {
    defines.DATA_USE_FLOAT = 1;
  }
  if (textureFloatExt && analyser.getFloatFrequencyData) {
    defines.FREQ_USE_FLOAT = 1;
  }
  programs[name] = createProgram({
    name,
    shaders: [
      { type: gl.VERTEX_SHADER, src: VERTEX_SHADER },
      { type: gl.FRAGMENT_SHADER, src }
    ],
    uniforms: {
      screenSize: { type: '2fv', initial: new Float32Array([0, 0]) },
      data: { type: '1i', initial: 0 },
      freq: { type: '1i', initial: 1 },
      time: { type: '1f', initial: 0 },
      duration: { type: '1f', initial: 0 },
      globalTime: { type: '1f', initial: 0 }
    },
    attribs: ['position'],
    defines
  });
}
function draw({force=false} = {}) {
  if (!gl) return;
  if (audio.paused && !force) {
    vizRunning = false;
    return;
  }
  gl.clear(gl.COLOR_BUFFER_BIT);
  const program = currentProgram ? programs[currentProgram] || null : null;
  gl.useProgram(program);
  if (program) {
    if (!audio.paused) {
      gl.activeTexture(gl.TEXTURE0);
      if (textureFloatExt && analyser.getFloatTimeDomainData) {
        analyser.getFloatTimeDomainData(dataArray);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, frequencyBinCount, frequencyBinCount, 0, gl.LUMINANCE, gl.FLOAT, dataArray);
      } else {
        analyser.getByteTimeDomainData(dataArray);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, frequencyBinCount, frequencyBinCount, 0, gl.LUMINANCE, gl.UNSIGNED_BYTE, dataArray);
      }
      gl.activeTexture(gl.TEXTURE1);
      if (textureFloatExt && analyser.getFloatFrequencyData) {
        analyser.getFloatFrequencyData(freqArray);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, frequencyBinCount, frequencyBinCount, 0, gl.LUMINANCE, gl.FLOAT, freqArray);
      } else {
        analyser.getByteFrequencyData(freqArray);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, frequencyBinCount, frequencyBinCount, 0, gl.LUMINANCE, gl.UNSIGNED_BYTE, freqArray);
      }
    }
    setUniform(program.uniforms.screenSize, new Float32Array([canvas.width, canvas.height]));
    setUniform(program.uniforms.time, audio.currentTime);
    setUniform(program.uniforms.duration, audio.duration);
    setUniform(program.uniforms.globalTime, window.performance.now() / 1000);
    gl.drawArrays(gl.TRIANGLES, 0, 6);
  }
  if (!currentProgram) {
    vizRunning = false;
    return;
  }
  vizRunning = true;
  requestAnimationFrame(draw);
}

function displayWaveform(track) {
  if (!track.waveform) {
    elems.sliderWaveform.style.backgroundImage = null;
    const {peaks} = METADATA[track.id];
    const waveform = E('canvas', {id: 'waveform', width: peaks.length, height: WAVEFORM_HEIGHT});
    const ctx = waveform.getContext('2d');
    for (let x = 0; x < peaks.length; x += 1) {
      const PEAK_MAX = 128;
      const y = (1 - peaks[x] / PEAK_MAX) * waveform.height;
      ctx.fillRect(x, y, 1, waveform.height - y);
    }
    waveform.toBlob(blob => {
      track.waveformBlob = blob;
      track.waveform = `url(${URL.createObjectURL(blob)})`;
      displayWaveform(track);
    });
  } else if (trackOrder[trackOrderIndex] === track.id) {
    elems.sliderWaveform.style.backgroundImage = track.waveform;
  }
}
