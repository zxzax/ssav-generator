export { createElement };

function createElement(type, attrs = {}, ...children) {
  const elem = document.createElement(type);
  for (const key of Object.keys(attrs)) {
    const value = attrs[key];
    if (key.substr(0, 2) == 'on') {
      const eventName = key.substr(2).toLowerCase();
      elem.addEventListener(eventName, value);
    } else {
      if (key === 'value' && (elem instanceof HTMLInputElement || elem instanceof HTMLSelectElement || elem instanceof HTMLTextAreaElement)) {
        elem.value = value;
      } else if (key === 'style') {
        Object.assign(elem.style, value);
      } else if (key === 'checked') {
        elem.checked = value;
      } else {
        elem.setAttribute(key, value);
      }
    }
  }
  for (const c of children) {
    if (c instanceof Node) {
      elem.appendChild(c);
    } else {
      elem.appendChild(document.createTextNode(c));
    }
  }
  return elem;
}
