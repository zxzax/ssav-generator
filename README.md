# Static Site Audio Visualization Generator

Give it a [M3U](https://en.wikipedia.org/wiki/M3U) playlist and get out a static site with a cool visualizer.

```shell
git clone https://gitlab.com/meditator/ssav-generator
cd ssav-generator
npm install
node cli.js ~/my_album.m3u -o /var/www/html/album
```

### License

[Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)
