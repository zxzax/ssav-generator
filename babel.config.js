module.exports = api => {
  if (api) {
    api.cache.forever();
  }
  return {
    presets: [
      ["@babel/preset-env", {
        useBuiltIns: 'usage',
        corejs: '3.6'
      }]],
    plugins: [
      '@babel/plugin-transform-runtime'
    ]
  };
};
