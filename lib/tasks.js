/*
 * This file is licensed to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

const util = require('util');
const { statExists } = require('./fs.js');
const { execFile } = require('child_process');
const execFilep = util.promisify(execFile);
const { finished } = require('stream');
const finishedp = util.promisify(finished);
const path = require('path');
const fs = require('fs');
const fsp = fs.promises;
const webpack = require('webpack');
const archiver = require('archiver');
const ffmpegPath = require('ffmpeg-static');

module.exports = {
  copyFile,
  mkdir,
  render,
  webpackCompile,
  generateMetadata,
  combineMetadata,
  zipFiles
};

async function copyFile(dest, src) {
  await fsp.copyFile(src, dest);
  console.log(`${src} -> ${dest}`);
}

async function mkdir(dir) {
  const stat = await statExists(dir);
  if (stat && stat.isDirectory()) {
    throw new Error(`${dir} exists and is not a directory`);
  } else if (!stat) {
    await fsp.mkdir(dir, {recursive: true});
    console.log(`mkdir ${dir}`);
  }
}

function render(func) {
  return async (dest, src, tpl) => {
    await fsp.writeFile(dest, func());
    console.log(`ejs ${path.basename(tpl)} ${path.basename(src)} -> ${dest}`);
  };
};

function splitErrors(errors) {
  return errors.map(e => e.split('\n').map(l => `  ${l}`).join('\n')).join('\n');
}

function webpackCompile(config, input) {
  return async dest => {
    await new Promise(
      (resolve, reject) =>
      webpack(config, (err, stats) => {
        if (err) {
          reject(err);
        } else {
          const info = stats.toJson();
          if (stats.hasWarnings()) {
            console.warn(
              [`Webpack returned ${info.warnings.length} warning(s):`,
                splitErrors(info.warnings)].join('\n\n'));
          }
          if (stats.hasErrors()) {
            reject(new Error(
              [`Webpack returned ${info.errors.length} error(s):`,
                splitErrors(info.errors)].join('\n\n')));
          } else {
            resolve(stats);
          }
        }
      }));
    console.log(`webpack ${input.join(' ')} -> ${dest}`);
  };
}

async function generatePeaks(file) {
  const ffmpeg = execFilep(
    ffmpegPath, [
      '-loglevel', 'warning', '-i', file,
      '-ac', '1', '-f', 's8', '-'],
      {encoding: 'buffer', maxBuffer: Infinity});
  const {stdout, stderr} = await ffmpeg;
  if (ffmpeg.child.exitCode != 0) {
    throw `ffmpeg returned ${ffmpeg.child.exitCode}, stderr:\n${stderr}`;
  }
  const PEAK_COUNT = 2048;
  const sampleSize = stdout.length / PEAK_COUNT;
  const peaks = Array(PEAK_COUNT).fill(0n);
  const counts = Array(PEAK_COUNT).fill(0);

  let bytesRead = 0;
  const bps = 1;
  for (let i = 0; i < stdout.length; i += bps) {
    const sample = Math.floor(bytesRead / sampleSize);
    const v = stdout.readInt8(i);
    peaks[sample] += BigInt(v * v);
    counts[sample] += 1;
    bytesRead += bps;
  }
  let max = 0;
  for (let i = 0; i < peaks.length; i += 1) {
    peaks[i] = Math.sqrt(Number(peaks[i] / BigInt(counts[i])));
    max = Math.max(Math.abs(peaks[i]), max);
  }
  for (let i = 0; i < peaks.length; i += 1) {
    peaks[i] = Math.round(peaks[i] / max * 128);
  }
  return peaks;
}

async function generateMetadata(dest, src) {
  const metadata = {
    peaks: await generatePeaks(src)
  };
  await fsp.writeFile(dest, JSON.stringify(metadata));
  console.log(`metadata ${src} -> ${dest}`);
}

function combineMetadata(files) {
  return async dest => {
    const metadatas = await Promise.all(files.map(
        f => fsp.readFile(f).then(j => JSON.parse(j))));
    await fsp.writeFile(dest, JSON.stringify(metadatas));
    console.log(`combine metadata ${dest}`);
  };
}

function zipFiles(files) {
  return async dest => {
    await new Promise((resolve, reject) => {
      var output = fs.createWriteStream(dest);
      var archive = archiver('zip', { zlib: { level: 9 } });
      output.on('close', () => resolve());
      archive.on('error', e => reject(e));
      archive.pipe(output);
      for (const file of files) {
        archive.file(file, { name: path.basename(file) });
      }
      archive.finalize();
    });
    console.log(`create zip ${dest}`);
  };
}
