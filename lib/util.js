module.exports = {
  clamp,
  formatDuration,
  mapObject,
  promiseAllObject,
  shuffle
};

function clamp(a, min, max) {
  return Math.max(Math.min(a, max), min);
}

function formatDuration(d) {
  return `${Math.floor(d / 60)}:${Math.round(d % 60).toString().padStart(2, '0')}`;
}

function mapObject(obj, func) {
  return Object.entries(obj).reduce((o, [k, v]) => (o[k] = func(v, k), o), {});
}

function promiseAllObject(obj) {
  const keys = Object.keys(obj);
  return Promise.all(keys.map(k => Promise.resolve(obj[k])))
    .then(results => results.reduce((o, r, i) => (o[keys[i]] = r, o), {}));
}

function shuffle(array) {
  for (let i = array.length - 1; i > 0; i -= 1) {
    const r = Math.floor(Math.random() * (i + 1));
    [array[i], array[r]] = [array[r], array[i]];
  }
  return array;
}
