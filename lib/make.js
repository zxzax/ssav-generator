/*
 * This file is licensed to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

const { statExists } = require('./fs.js');

module.exports = class Make {
  constructor() {
    this.targets = new Map;
  }
  add(out, deps, options = {}) {
    const target = this.targets.get(out) || { out, deps: [] };
    Object.assign(target, options);
    target.deps.push(...(Array.isArray(deps) ? deps : [deps]).map(d => {
      if (typeof d === 'function') {
        const f = () => d(out, ...target.deps);
        f.target = out;
        f.phony = !!target.phony;
        return f;
      }
      return d;
    }));
    this.targets.set(out, target);
  }
  async exec(out) {
    const getDeps = (o, q=[]) => {
      const t = this.targets.get(o);
      if (t) {
        for (const dep of t.deps) {
          getDeps(dep, q);
        }
      }
      if (!q.includes(o)) {
        q.push(o);
      }
      return q;
    };

    const queue = getDeps(out);
    let tasksBuilt = 0;
    for (let depIndex = 0; depIndex < queue.length; depIndex += 1) {
      const dep = queue[depIndex];
      if (typeof dep === 'function') {
        let needsBuild = dep.phony;
        if (!dep.phony) {
          const targetStat = await statExists(dep.target);
          if (!targetStat) {
            if (this.debug) {
              console.log(`${dep.target} doesn't exist`);
            }
            needsBuild = true;
          } else if (!targetStat.isDirectory()) {
            for (const subdep of getDeps(dep.target)) {
              const name = typeof subdep === 'function' ? subdep.target : subdep;
              if (!subdep.phony && name) {
                const subStat = await statExists(name);
                if (subStat && !subStat.isDirectory() && subStat.mtimeMs > targetStat.mtimeMs) {
                  if (this.debug) {
                    console.log(`${dep.target} outdated due to ${name}`);
                  }
                  needsBuild = true;
                  break;
                }
              }
            }
          }
        }
        if (needsBuild) {
          await Promise.resolve(dep());
          tasksBuilt += 1;
          if (dep.phony) {
            getDeps(dep.target, queue);
          }
        }
      }
    }
    if (tasksBuilt === 0) {
      console.log('All files up to date.');
    }
  }
};

