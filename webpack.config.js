/* eslint-env node */
const path = require('path');
const webpack = require('webpack');
const babelOptions = require('./babel.config.js')();

module.exports = env => ({
  entry: ['./src/player.js'],
  output: {
    filename: 'player.js'
  },
  mode: env === 'production' ? env : 'development',
  target: 'web',
  devtool: env === 'production' ? false : 'source-map',
  resolve: {
    alias: {}
  },
  module: {
    rules: [
    {
      test: /\.js$/,
      include: [
        path.resolve(__dirname, 'src')
      ],
      use: [{
        loader: 'babel-loader',
        options: Object.assign({
          cacheDirectory: true
        }, babelOptions)
      }],
      sideEffects: false
    },
    {
      test: /\.(frag|vert|glsl|html)$/,
      use: 'raw-loader'
    },
    {
      test: /.*LICENSE.*/,
      use: 'raw-loader'
    }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({PRODUCTION: env === 'production'}),
  ]
});
